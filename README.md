# Usage
* `pipe i ...`: Installs package

* `pipe r ...`: Uninstalls package

* `pipe u ...` : Upgrades package

* `pipe l ...` : Lists all packages

* `pipe h ...` : For help
